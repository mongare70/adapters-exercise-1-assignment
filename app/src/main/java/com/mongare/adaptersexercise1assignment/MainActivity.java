package com.mongare.adaptersexercise1assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    private EditText mName, mPassword;
    private Spinner mProgram;
    private Button mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declaring views
        mName = (EditText) findViewById(R.id.editTextName);
        mPassword = (EditText) findViewById(R.id.editTextPassword);
        mProgram = (Spinner) findViewById(R.id.spinnerProgram);
        mRegister = (Button) findViewById(R.id.btnRegister);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, getResources()
        .getStringArray(R.array.program_array));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mProgram.setAdapter(adapter);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String name = mName.getText().toString();
                String program = mProgram.getSelectedItem().toString();

                if (!name.equals("")) {
                    Log.d("Adapters", "The name is: " + name);
                    Log.d("Adapters", "The selected program is: " + program);
                } else {
                    Log.d("Adapters","Enter your name first");
                }
            }
        });



    }
}